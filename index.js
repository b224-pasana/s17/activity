console.log("HELLO WORLD!!!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
		function printWelcomeMessage() {
					let name = prompt("What is your name?")
					let age = prompt("How old are you?")
					let location = prompt("Where city do you live?")
				

				console.log("Hello, " + name);
				console.log("You are " + age + " years old.");
				console.log("You live in " + location + " City.");

				};

				printWelcomeMessage();
				alert("Thank you for your input! :)")
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

	
*/

	//second function here:
				function faveArtists(){
					let musicArtist1 = "Kristin Chenoweth";
					let musicArtist2 = "Idina Menzel";
					let musicArtist3 = "Lin-Manuel Miranda";
					let musicArtist4 = "Philipa Soo";
					let musicArtist5 = "Lea Salonga";

					console.log("1. " + musicArtist1);
					console.log("2. " + musicArtist2);
					console.log("3. " + musicArtist3);
					console.log("4. " + musicArtist4);
					console.log("5. " + musicArtist5);
				};

				faveArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
				function faveMovies(){
					let movie1 = "The Sound of Music";
					let movie2 = "The Notebook";
					let movie3 = "Avengers: Endgame (2019)";
					let movie4 = "The White Ribbon";
					let movie5 = "Demon Slayer and the Mugen Train";
					let rating1 = 83;
					let rating2 = 53;
					let rating3 = 94;
					let rating4 = 86;
					let rating5 = 98;


						console.log("1. " + movie1);
						console.log("Rotten Tomatoes Rating: " + rating1 + "%");
						console.log("2. " + movie2);
						console.log("Rotten Tomatoes Rating: " + rating2 + "%");
						console.log("3. " + movie3);
						console.log("Rotten Tomatoes Rating: " + rating3 + "%");
						console.log("4. " + movie4);
						console.log("Rotten Tomatoes Rating: " + rating4 + "%");
						console.log("5. " + movie5);
						console.log("Rotten Tomatoes Rating: " + rating5 + "%");
					};

					faveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


		let printFriends = function printUsers(){
			alert("Hi! Please add the names of your friends.");
			let friend1 = prompt("Enter your first friend's name:"); 
			let friend2 = prompt("Enter your second friend's name:"); 
			let friend3 = prompt("Enter your third friend's name:");

			console.log("You are friends with:");
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
			};

			printFriends();
